package me.wertik.stackprevent;

import me.wertik.stackprevent.commands.StackPreventCommand;
import me.wertik.stackprevent.listeners.ItemMergeListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class Main extends JavaPlugin {

    private static Main instance;

    public static Main getInstance() {return instance;}

    private ConfigLoader configLoader;

    public List<String> disabledTypes;
    private boolean enabled;

    @Override
    public void onEnable() {
        info("§f-------------");

        instance = this;
        configLoader = new ConfigLoader();
        getServer().getPluginManager().registerEvents(new ItemMergeListener(), this);
        getCommand("stackprevent").setExecutor(new StackPreventCommand());
        getCommand("sp").setExecutor(new StackPreventCommand());
        info("§aClasses loaded");

        configLoader.loadYamls();
        disabledTypes = configLoader.loadTypeList();
        enabled = configLoader.loadEnabled();
        info("§aConfig loaded");

        info("§f-------------");
    }

    @Override
    public void onDisable() {
        configLoader.save();
    }

    private void info(String msg) {
        getServer().getConsoleSender().sendMessage("§f["+getDescription().getName()+"] " + msg);
    }

    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    public boolean isFunctionEnabled() {
        return enabled;
    }

    public void switchFunction() {
        this.enabled = this.enabled ? false : true;
    }
}
