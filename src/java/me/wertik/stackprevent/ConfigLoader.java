package me.wertik.stackprevent;

import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.util.List;

public class ConfigLoader {

    private Main plugin;
    private FileConfiguration config;

    public ConfigLoader() {
        plugin = Main.getInstance();
    }

    public void loadYamls() {

        // CF
        File configFile = new File(plugin.getDataFolder() + "/config.yml");

        if (!configFile.exists()) {
            plugin.getConfig().options().copyDefaults(true);
            plugin.saveConfig();
            plugin.getServer().getConsoleSender().sendMessage(plugin.getDescription().getName() + "§aGenerated default §f" + configFile.getName());
        }

        config = plugin.getConfig();
    }

    public List<String> loadTypeList() {
        return config.getStringList("item-types");
    }

    public boolean loadEnabled() {
        return config.getBoolean("enabled");
    }

    public void save() {
        config.set("item-types", Main.getInstance().disabledTypes);
        config.set("enabled", Main.getInstance().isFunctionEnabled());
        plugin.saveConfig();
    }
}
