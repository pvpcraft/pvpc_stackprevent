package me.wertik.stackprevent.commands;

import me.wertik.stackprevent.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class StackPreventCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args.length == 0) {
            sender.sendMessage("§8§m----§r §eStackPrevent Help Page §8§m----§r");
            sender.sendMessage("§e/stackprevent list §8- §7List disabled item types." +
                    "\n§e/stackprevent add <itemType> §8- §7Add an item type to the list." +
                    "\n§e/stackprevent remove <itemType> §8- §7Remove an item type from the list." +
                    "\n§e/stackprevent switch §8- §7Turn the plugin function on/off.");
            return false;
        }

        switch (args[0]) {
            case "add":
                if (args.length > 2) {
                    sender.sendMessage("§cToo many args. /stackprevent add <itemType>");
                    return false;
                } else if (args.length < 2) {
                    sender.sendMessage("§cNot enough arguments. /stackprevent add <itemType>");
                    return false;
                }

                if (!Main.getInstance().disabledTypes.contains(args[1])) {
                    Main.getInstance().disabledTypes.add(args[1].toUpperCase());
                    sender.sendMessage("§aAdded §f" + args[1]);
                } else
                    sender.sendMessage("§cThat item is already in the list.");
                break;
            case "remove":
                if (args.length > 2) {
                    sender.sendMessage("§cToo many args. /stackprevent remove <itemType>");
                    return false;
                } else if (args.length < 2) {
                    sender.sendMessage("§cNot enough arguments. /stackprevent remove <itemType>");
                    return false;
                }

                if (Main.getInstance().disabledTypes.contains(args[1])) {
                    String type = args[1].toUpperCase();
                    Main.getInstance().disabledTypes.remove(type);
                    sender.sendMessage("§eRemoved §f" + args[1]);
                } else sender.sendMessage("§cThat item type is not in the list.");
                break;
            case "list":
                if (args.length > 2) {
                    sender.sendMessage("§cToo many args. /stackprevent list");
                    return false;
                }
                String types = "§eTypes:§f";
                for (String type : Main.getInstance().disabledTypes) {
                    if (Main.getInstance().disabledTypes.indexOf(type) == (Main.getInstance().disabledTypes.size()-1)) {
                        types = types + " " + type + ".";
                        break;
                    }
                    types = types + " " + type + ",";
                }
                sender.sendMessage(types);
                break;
            case "switch":
                if (args.length > 2) {
                    sender.sendMessage("§cToo many args. /stackprevent switch");
                    return false;
                }
                Main.getInstance().switchFunction();
                sender.sendMessage("§eSwitched to §f" + Main.getInstance().isFunctionEnabled());
                break;
        }

        return false;
    }
}
