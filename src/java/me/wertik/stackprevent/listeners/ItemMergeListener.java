package me.wertik.stackprevent.listeners;

import me.wertik.stackprevent.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;

public class ItemMergeListener implements Listener {

    public ItemMergeListener() {
    }

    @EventHandler
    public void onMerge(ItemMergeEvent e) {
        if (Main.getInstance().isFunctionEnabled())
            if (Main.getInstance().disabledTypes.contains(e.getEntity().getItemStack().getType().toString()))
                e.setCancelled(true);
    }
}
